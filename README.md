# chcore

os chcore lab

## 项目内容

本次参赛作品主要通过完成对操作系统的lab（模块函数）来对操作系统有一个更加深刻的学习
### lab1：机器启动

1.通过阅读文档来学习一些ARM汇编指令以及一些其他基础教学

2.启动qemu以及使用gdb来跟踪操作系统启动时第一个函数的入口地址(bootloader)

3.寻找build/kernel.image入口文件

4.在单核机器上单步调试程序的执行过程

5.查看build/kernel.img的objdump信息，对比VMA与LMA的差异

6.实现并完善内核代码的打印功能(kernel/common/printk.h)

7.寻找内核栈初始化的函数并理解内核栈在内存的位置，内核如何为栈保留空间

8.通过gdb找到stack_test的函数地址，理解该函数的调用过程

9.寻找函数返回或被调用时幀指针的值如何存放在栈中

10.将在kernel/monitor.c文件中实现打印出函数调用时栈中的地址信息，理解调用的过程

### lab2：内存管理

1.理解物理内存布局

2.实现kernel/mm/buddy.c的伙伴系统

3.虚拟内存管理分析理解虚拟地址翻译的过程

4.实现kernel/mm/page_table.c页表管理部分的代码

5.完成对内核空间的映射部分代码（kernel/mm/mm.c）


### lab3:用户进程的异常处理


1.实现用户进程的init过程（kernel/process/process.c&&thread.h）

2.实现对异常的处理流程（kernel/sched）

